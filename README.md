# SCPeduler - Scheduling by Constraint Programing (HOS-ML Phase 2)
[![pipeline status](https://gitlab.com/edvardholden/scpeduler/badges/master/pipeline.svg)](https://gitlab.com/edvardholden/casc-j10/-/commits/master)
[![coverage report](https://gitlab.com/edvardholden/scpeduler/badges/master/coverage.svg)](https://gitlab.com/edvardholden/casc-j10/-/commits/master)



Program for automatically constructing optimal and effective heuristic schedules via constraint programming.
The program is a part of [HOS-ML](https://gitlab.com/edvardholden/hos-ml), a three-phase system for discovering
and selecting heuristics for heterogeneous (diverse) sets of problems. 
Although the system was designed for automated theorem provers in first-order logic, it is a general
purpose heuristic/strategy optimisation tool.

## Installation

This project expects python3 (3.11 >=)

Install SCPeduler by running:
```bash
git clone git@gitlab.com:edvardholden/scpeduler.git
pip3 install -r requirements.txt
pip3 install -e .
```

### For developers

Install the additional development packages:
```bash
pip3 install -r requirements_dev.txt
```
The test suite and quality verification setup uses `tox`.


## Getting Started

To construct a schedule, you need to specify the heuristic data and the global timeout for the schedule.
```bash
python3 compute_schedules.py [cvs-data] [global-timeout] 
```

Example of learning a schedule for the dataset 'qbfeval_dqbf_small_data.csv' with a timeout of 5
```bash
python3 compute_schedules.py data/heuristics/qbfeval_dqbf_small_data.csv 5
```

This results in a heuristic schedule $[[(h_0, t_0),..,(h_n, t_n)]]$ where $h_0$ is the first heuristic in 
the schedule running on the first core with a runtime of $t_0$.


See --help for more information about specifying options, like e.g. the number of cores for the schedule.
```bash
python3 compute_schedules.py --help
```



### Data Format

SCPeduler supports evaluation data in a csv format where the columns are heuristic and the rows are problem.
The evaluation entries represent the solving time of a heuristic-problem pair in seconds.
All values that are zero and larger represent the success of a problem attempt.
The special value -1 denotes unsolved instances, and -2 represents incorrect solutions.

For iProver developers, evaluation data can be downloaded from the cluster:
```bash
python3 download_evaluation_data.py
```
The script expects the database credential to be in a dictionary in `db_cred.py` in the project directory.


### Multi-Core Schedules

The number of cores the schedule is allowed to use can be specified with `--no_cores` (default is 1).
A multicore schedule is the list of schedules $[s_0], ... , [s_m]$ where each schedule $s$ runs in parallel on each core.
The wallclock of the schedule is the global-timeout, and the cpu time is expected to be: global-timeout*no-cores.
The upper runtime of individual heuristics can be limited by setting `--max_heuristic_runtime`.

### Evaluation

Test data can be provided as a csv file to `--test_data_file [test-csv]`.
This will evaluate the schedule performance on the test problems.
The heuristics occurring in the schedule must all occur in the test data.



### Admissible Schedules

You can create admissible schedules over the dataset if the main project `hos-ml` is installed.
This results in a set of schedules where each schedule is optimised for specific types of problems, where
the problem type is defined over behavioural properties.



## BibTeX Citation

If you use SCPeduler, please cite:

```
@inproceedings{DBLP:conf/mkm/HoldenK21,
  author       = {Edvard K. Holden and
                  Konstantin Korovin},
  editor       = {Fairouz Kamareddine and
                  Claudio Sacerdoti Coen},
  title        = {Heterogeneous Heuristic Optimisation and Scheduling for First-Order
                  Theorem Proving},
  booktitle    = {Intelligent Computer Mathematics - 14th International Conference,
                  {CICM} 2021, Timisoara, Romania, July 26-31, 2021, Proceedings},
  series       = {Lecture Notes in Computer Science},
  volume       = {12833},
  pages        = {107--123},
  publisher    = {Springer},
  year         = {2021},
  url          = {https://doi.org/10.1007/978-3-030-81097-9\_8},
  doi          = {10.1007/978-3-030-81097-9\_8},
  timestamp    = {Thu, 29 Jul 2021 13:42:12 +0200},
  biburl       = {https://dblp.org/rec/conf/mkm/HoldenK21.bib},
  bibsource    = {dblp computer science bibliography, https://dblp.org}
}
```

## Authors

* **Edvard Holden** 

See also the list of [contributors](https://gitlab.com/edvardholden/scpeduler/-/graphs/master) who participated in this project.


