import sys
import logging

from scpeduler.evaluate import run_evaluation
from scpeduler.input_parser import get_compute_input_args
from scpeduler.data import load_and_process_evaluation_data
from scpeduler.schedule import (
    convert_solution_to_schedule,
    report_schedule,
)
from scpeduler.solver import solve_scheduling_problem
from scpeduler.analyse import report_evaluation_data_stats

log = logging.getLogger()


def main() -> None:
    # Parse the input arguments
    args = get_compute_input_args(sys.argv[1:])

    # Set the loglevel
    log.setLevel(args.loglevel)
    log.debug(str(args))

    # Set time upper bound
    if args.max_heuristic_runtime is None:
        args.max_heuristic_runtime = args.global_timeout

    # Load data for computing schedule
    evaluation_data = load_and_process_evaluation_data(
        args.evaluation_data,
        args.mask_lower,
        args.max_heuristic_runtime,
        args.keep_unsound_heuristics,
        args.exclude_heuristics,
        args.remove_dominated_heuristics,
        args.admissible_sc,
        args.admissible_sf,
    )

    # Report overall statistics on the dataset
    if args.report_evaluation_data_stats:
        report_evaluation_data_stats(evaluation_data)

    # Compute the solution to the scheduling problem specified by the evaluation data
    solution = solve_scheduling_problem(
        args.solver_workers,
        args.solver_max_time,
        evaluation_data,
        args.global_timeout,
        args.max_heuristic_runtime,
        args.no_schedule_cores,
    )

    # Interpret the assigned variables as a schedule
    schedule = convert_solution_to_schedule(solution, args.global_timeout)

    # Report the final schedule
    report_schedule(schedule, args.print_deploy_string)

    print("\n\n\n## Performance Evaluation ##")
    run_evaluation(
        schedule,
        evaluation_data,
        args.global_timeout,
        args.test_data_file,
        args.evaluation_data,
        args.save_json_result,
    )


if __name__ == "__main__":
    main()
