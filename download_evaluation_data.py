"""
Script for downloading evaluation data from the iProver experimental clusters.
It operates on heuristic sets defined in download_data_config.py
"""
from pathlib import Path
from typing import List
import mysql.connector as db
import numpy as np
import csv
import traceback
import sys
import os
import socket

from scpeduler.input_parser import get_download_input_args
from download_data_config import data_set_mapper


UNSOLVED_PROBLEM_EVALUATION_VALUE = "-1"
INCORRECT_PROBLEM_EVALUATION_VALUE = "-2"
DATA_DIR = "data/"


def main():
    # Parse the input arguments
    args = get_download_input_args(sys.argv[1:])

    # Get the list of heuristics in the heuristic set
    heuristics = get_heuristics_from_set_name(args.heuristic_set)
    print(f"Number of heuristics: {len(heuristics)}")

    # Get the features from the db
    evaluation_data = get_experiment_results(heuristics, args.ltb)

    # Get an apt name and write the data to a csv file
    file_path = get_resulting_file_path(args.heuristic_set)
    save_evaluation_data_file(evaluation_data, heuristics, file_path)


def get_heuristics_from_set_name(heuristic_sets: str) -> List[str]:
    # List for storing the heuristics
    heuristics: List[str | int] = []

    # Extract the heuristics from the dictionary
    for name in heuristic_sets:
        try:
            heuristics.extend(data_set_mapper[name])
        except ValueError:
            print(f"Could not find heuristic set {name} in the dataset config")
            print("Exiting...")
            sys.exit(1)

    # Make sure all heuristics are represented as strings
    heuristics = [str(h) for h in heuristics]

    # Map to strings, remove duplicates and sort
    return sorted(set(map(str, heuristics)))


def get_resulting_file_path(heuristic_set: List[str]) -> str:
    # Check that directory exists
    os.makedirs(os.path.dirname(DATA_DIR), exist_ok=True)

    # Make file name based on the heuristic set names
    file_base = "_".join(sorted(Path(hset).stem for hset in heuristic_set))
    file_path = os.path.join(DATA_DIR, f"{file_base}.csv")
    return file_path


def get_hostname() -> str:
    hostname = socket.gethostname().split(".")[0]
    return hostname


def save_evaluation_data_file(
    evaluation_data, heuristics: List[str], file_path: str
) -> None:
    # Get hostname as prefix
    hostname = get_hostname()

    # Set the columns to be hostname+experiment_ID to avoid clashes between servers
    column_header = [f"{hostname}_{heur}" for heur in heuristics]

    # Write the evaluation data to file
    with open(file_path, "w+", newline="") as f:
        # Make a CSV writer
        a = csv.writer(f, delimiter=",")
        # Write the header
        a.writerow(["ProblemNames"] + column_header)
        # Write the problem names/ids + evaluation data
        a.writerows(evaluation_data)

    print(f"Data file saved as: {file_path}")


def get_db_connection():
    # Connect to the database
    try:
        import db_cred
    except ImportError as err:
        print(err)
        print("Need to add database credentials dictionary in a db_cred file")
        sys.exit(1)

    # Get database connection details
    db_connection_details = db_cred.db_connection_details
    conn = db.connect(**db_connection_details)
    curs = conn.cursor()
    return conn, curs


def get_problem_names(heuristic: str):
    # Get the problem names of a heuristic
    conn = curs = None
    try:
        conn, curs = get_db_connection()

        # Get the problem names from the experiment
        curs.execute(
            f"""
                     SELECT Filename
                     FROM ProblemVersion PV
                     INNER JOIN ProblemRun PR
                     ON PR.Problem = PV.ProblemVersionID
                     WHERE PR.Experiment={heuristic}
                     ORDER BY PR.Problem
                     ;""",
        )

        # Get the problem names
        res = curs.fetchall()
        problem_names = [p[0] for p in res]
        return problem_names

    except Exception as err:
        print(err)
        print(traceback.format_exc())
    finally:
        if curs:
            curs.close()
        if conn:
            conn.close()


def get_experiment_results(heuristics: List[str], is_ltb: bool):
    # Get the problem names from the first experiment and transpose to the desired format
    problem_names = get_problem_names(heuristics[0])
    print("Number of problems: ", len(problem_names))
    problem_names = np.asarray([problem_names]).T

    # Get the heuristic evaluation data of each heuristic
    heuristic_features = []
    for heur in heuristics:
        res = query_heuristic_evaluation(heur, is_ltb)

        # Check that the number of problems in the experiment is correct
        if len(res) != len(problem_names):
            raise ValueError(
                f"Error: Experiment {heur} contains {len(res)} instances "
                f"while there are {len(problem_names)} problems files"
            )
        heuristic_features += [res]

    # Transpose the data according to the desired format
    evaluation = np.asarray(heuristic_features).T
    evaluation = np.asarray(evaluation[0])

    # Concatenate the problem names with their runtimes
    data = np.concatenate((problem_names, evaluation), axis=1)
    return data


def query_heuristic_evaluation(exp_id: str, is_ltb: bool):
    # Get the evaluation result of a heuristic. If the result is an unsolved instance: UNSOLVED_PROBLEM_EVALUATION_VALUE
    # If the result is incorrect: INCORRECT_PROBLEM_EVALUATION_VALUE. Otherwise, the runtime rounded to 2 decimals

    conn = curs = None
    try:
        conn, curs = get_db_connection()

        # Start building a case query of select, by first setting
        # the errors and timeout to zero
        sql_query = """
            SELECT
            (
                CASE
                WHEN (SR.Solved=0) THEN {0}
            """.format(
            UNSOLVED_PROBLEM_EVALUATION_VALUE
        )

        # Separate cases for LTB/Non LTB
        if is_ltb:
            # Set the incorrect problems to the appropriate value
            sql_query += """
                WHEN (SL.Sat and SR.Unsat) THEN {0}
                """.format(
                INCORRECT_PROBLEM_EVALUATION_VALUE
            )

            # Only include unsat problems and set all to NULL
            sql_query += """
               WHEN (PR.SZS_Status = 'Theorem' OR PR.SZS_Status = 'Unsatisfiable') THEN ROUND(PR.Runtime, 2)
               ELSE {0}
                         """.format(
                UNSOLVED_PROBLEM_EVALUATION_VALUE
            )
        else:
            # Set the incorrect problems to the appropriate value
            sql_query += """
                WHEN ((SL.Unsat and SR.Sat) OR (SL.Sat and SR.Unsat)) THEN {0}
                """.format(
                INCORRECT_PROBLEM_EVALUATION_VALUE
            )
            # Non-ltb, we include all statuses
            sql_query += """
                ELSE ROUND(PR.Runtime, 2)
                         """
        # Case statement needs to have a variable otherwise a library throws a unicode error
        sql_query += """
             END
             ) as res
                     """

        # Get the result from the problem run
        sql_query += """
            FROM ProblemRun PR
                     """

        # Get Status information
        sql_query += """
            join ProblemVersion PV ON PV.ProblemVersionID = PR.Problem
            join SZSStatus SL ON PV.Status = SL.SZSStatusID
            left join SZSStatus SR ON PR.Status = SR.SZSStatusID
                    """

        # Get experiment and order
        sql_query += """
            WHERE PR.Experiment={0}
            ORDER BY PR.Problem
                    """.format(
            exp_id
        )

        # Execute and fetch
        sql_query += ";"
        curs.execute(sql_query)
        res = curs.fetchall()
        return res

    except Exception as err:
        print(err)
        print(traceback.format_exc())
    finally:
        if curs:
            curs.close()
        if conn:
            conn.close()


if __name__ == "__main__":
    main()
