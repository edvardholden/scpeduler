echo "GLOBAL Schedule"
python3 -u scpeduler/scpeduler.py data/sup_cicm_20_train_new.csv 20 --test_data_file data/sup_cicm_20_test_new.csv --output_solved_json --max_heuristic_runtime 20

echo "ADMISSIBLE Schedule"
python3 -u scpeduler/scpeduler.py data/sup_cicm_20_train_new.csv 20 --test_data_file data/sup_cicm_20_test_new.csv --output_solved_json --max_heuristic_runtime 20 --admissible_filter_rate 0.95 --admissible_sc 1 --admissible_sf 1.0 --admissible

echo "FINITO"


