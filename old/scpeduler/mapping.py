import config
from analyser import compute_total_solved_problems
import sys


def map_problems_to_clusters(heuristic_data, heuristics, cluster_centers, admissible_sc, admissible_sf):

    # Function imports
    sys.path.append("/home/mbax4eh2/iprover-smac/wrappers")
    from cluster import compute_admissible_evaluation
    from sklearn.metrics.pairwise import pairwise_distances_argmin
    from admissible_mapping.distance import common_solved

    # We get teh admissible representation for each problem with a known
    # solution based on the current evaluation data

    # Get all the problems that are solved in the evaluation
    solved_problems = sorted(compute_total_solved_problems(heuristic_data, heuristics))

    # Compute admissible evaluation
    adm_eval = compute_admissible_evaluation(
        heuristic_data[heuristics].loc[solved_problems].replace({-1: None}),
        adm_fact=admissible_sf,
        adm_const=admissible_sc,
    )

    pred_clusters = pairwise_distances_argmin(adm_eval, Y=cluster_centers, metric=common_solved)

    # Create dict mapping between problem and each cluster
    cluster_map = {k: v for k, v in zip(solved_problems, pred_clusters)}

    if config.verbose or config.debug:
        print()
        print("Computed Cluster Mapping for {0} problems".format(len(solved_problems)))

    return cluster_map
