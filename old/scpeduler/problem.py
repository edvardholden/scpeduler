# Class for representing FOF problems from dataframes
import sys
import config


def get_problem_set(df, adm_const, adm_fact):
    # Let's construct the problem set based on the evaluation
    problems = []
    for prob in df.index:
        problem = Problem(prob, dict(df.loc[prob]))

        eval_vec = df.loc[prob].replace({-1: None}).values
        if config.ADMISSIBLE:
            adm_vec = compute_admissible_evaluation(
                [eval_vec], adm_fact=adm_fact, adm_const=adm_const
            )
            problem.embedding = adm_vec

        problems.append(problem)

    return problems
