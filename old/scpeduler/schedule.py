# Class for modelling a heuristic schedule

from sklearn.metrics.pairwise import pairwise_distances_argmin


class AdmissibleSchedule:
    def __init__(self):
        self.schedules = dict()
        self.cluster_centers = []

    def __str__(self):
        if len(self.schedules) == 0:
            return "Empty schedule"
        else:
            sched_str = ""
            for k in sorted(self.schedules.keys()):
                sched_str += "{0} : {1}\n".format(
                    list(self.cluster_centers[k]), self.schedules[k]
                )
            return sched_str

    def print_deploy_string(self):
        if len(self.schedules) == 0:
            print("Empty schedule")
        else:
            index = sorted(self.schedules.keys())

            for i in index:
                print(str(list(self.cluster_centers[i])) + ",")
            print()

            for n, i in enumerate(index):
                print("{0}: ".format(n), end="")
                self.schedules[i].print_deploy_string(end="")
                print(",")

    def add_schedule(self, local_schedule, cluster_center):
        self.schedules[len(self.schedules)] = local_schedule
        self.cluster_centers += [cluster_center]

        return self

    def attempt(self, problem):
        # Map to cluster!
        cluster = self.map_problem(problem)

        res = self.schedules[cluster].attempt(problem)
        res.set_schedule(cluster)

        return res

    def attempt_problems(self, problems):
        attempts = []
        for problem in problems:
            attempts.append(self.attempt(problem))

        return attempts

    def map_problem(self, problem):
        # If no evaluation vector is set
        if problem.embedding is None:
            cluster = 0  # DEFAULT
        else:
            cluster = self.compute_closest_cluster(problem.embedding)

        return cluster

    def compute_closest_cluster(self, adm_vec):
        pred = pairwise_distances_argmin(
            adm_vec, Y=self.cluster_centers, metric=common_solved
        )[0]
        return pred
