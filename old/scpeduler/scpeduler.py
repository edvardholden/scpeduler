import sys
import json
from schedule import Schedule, AdmissibleSchedule

# Setup root logger
import logging

logging.basicConfig(stream=sys.stdout, level=logging.INFO, format="%(message)s")
log = logging.getLogger()


def scpeduler(
    csv_file,
    global_timeout,
    no_cores=1,
    keep_unsound_heuristics=False,
    exclude=None,
    remove_dominated_heuristics=False,
    max_heuristic_runtime=None,
    mask_lower=None,
    solver_workers=4,
    solver_max_time=float("inf"),
    test_data_file=None,
    output_solved_json=False,
    admissible=False,
    admissible_sc=2.0,
    admissible_sf=1.5,
    admissible_filter_rate=None,
    admissible_filter_keep=True,
    admissible_no_clusters=None,
    loglevel=logging.INFO,
    print_deploy_string=False,
):
    # Compute the admissible clusters if admissible is set
    if admissible:
        # Compute the admissible schedules
        sys.path.append("/home/mbax4eh2/iprover-smac/wrappers")
        from cluster import compute_admissible_clusters

        log.info("Computing admissible clusters")
        clusters, cluster_centers = compute_admissible_clusters(
            evaluation.columns,
            evaluation_data=evaluation,
            adm_sc=admissible_sc,
            adm_sf=admissible_sf,
            adm_ratio=admissible_filter_rate,
            return_filtered=admissible_filter_keep,
            k_clusters=admissible_no_clusters,
        )
        # from t_res import clusters, cluster_centers

    # Compute schedule given the problem data
    if admissible:
        schedule = AdmissibleSchedule()
        for cluster_no in range(len(clusters)):
            local_schedule = compute_heuristic_schedule(
                solver_workers,
                solver_max_time,
                evaluation.loc[clusters[cluster_no]],
                global_timeout,
                max_heuristic_runtime,
                no_cores,
            )
            schedule.add_schedule(local_schedule, cluster_centers[cluster_no])
    else:
        schedule = compute_heuristic_schedule(
            solver_workers,
            solver_max_time,
            evaluation,
            global_timeout,
            max_heuristic_runtime,
            no_cores,
        )
