"""
Module for computing collections of statistics of datasets and schedules.
"""
from collections import Counter
from typing import Set, List

import numpy as np
import pandas as pd

import scpeduler.data as scpdata
from scpeduler.problem import ProblemAttempt
from scpeduler.stats import compute_top_n_heuristics, compute_greedy_min_cover


def report_evaluation_data_stats(data: pd.DataFrame) -> None:
    print()
    print("# " * 8)
    print("Dataset Evaluation Statistics")
    print()
    print("# Heuristic Totals #")

    # Compute total number of solved problems
    no_solved_problems = len(scpdata.remove_unsolved_problems(data))
    print(f"Total number of solved problems in data: {no_solved_problems}")
    print()

    # Compute the top three most solving heuristics overall
    print("Top n=3 most solving heuristics:")
    top_three_heuristics = compute_top_n_heuristics(data, n=3)
    top_three_solved: Set[str] = set()
    for heur, solved_set in top_three_heuristics:
        print(f"Heuristic {heur} :  Solved {len(solved_set)} ")
        top_three_solved = top_three_solved.union(solved_set)
    print(f"Problems solved in the union: {len(top_three_solved)}")

    print()
    print("# Greedy Min Cover # ")
    # Compute the greedy min cover
    cover, cover_solved = compute_greedy_min_cover(data)
    assert cover_solved == no_solved_problems
    print(f"Length of cover: {len(cover)}")
    print("Cover heuristics ordered by contribution:")
    print(" ".join(cover))
    print()

    # Report the union of the three most contributing heuristics
    top_cover_solved: Set[str] = set()
    for heur in cover[:3]:
        top_cover_solved = top_cover_solved.union(set(data.loc[data[heur] != -1].index))
    print(
        f"Number of problems solved in union by the three most contributing heuristics: {len(top_cover_solved)}"
    )
    print()
    print()


def compute_performance_stats(
    solver_result: List[ProblemAttempt], single_result: List[ProblemAttempt]
) -> None:
    # Compute basic and comparative statistics of solver vs single heuristic schedule
    # Compute basic statistics
    print("\n# Stats: Single Heuristic #")
    compute_basic_solved_stats(single_result)
    print("\n# Stats: Solver Schedule #")
    compute_basic_solved_stats(solver_result)

    # Compute the top 3 (or less) key heuristics in the solved results
    compute_best_schedule_heuristics(solver_result)

    print("\n# Solving times for percentages solved by the schedule #")
    compute_stats_solved_percentage(solver_result)

    # Compare experiments
    print("\n# Stats Compare #")
    compute_stats_compare(solver_result, single_result, "solver", "single")


def compute_basic_solved_stats(attempts: List[ProblemAttempt]) -> None:
    # Compute number of solved problems
    solved = [p for p in attempts if p.solved]
    print(f"Number of solved problems: {len(solved)}")

    # Compute average time
    average_time = np.average([p.runtime for p in solved]) if len(solved) > 0 else 0
    print(f"Average solving time: {average_time:.2f}")

    # Compute maximum time
    max_time = np.max([p.runtime for p in solved]) if len(solved) > 0 else 0
    print(f"Maximum solving time: {max_time:.2f}")


def compute_best_schedule_heuristics(schedule_result: List[ProblemAttempt]) -> None:
    heur_counter = Counter([p.solved_by for p in schedule_result if p.solved])
    print(
        f"Top most contributing heuristics for schedule: {heur_counter.most_common(3)}"
    )


def compute_stats_solved_percentage(schedule_result: List[ProblemAttempt]) -> None:
    # Compute solved times and order them for effective slicing
    times = sorted(p.runtime for p in schedule_result if p.solved)
    # The percentage intervals to investigate
    intervals = [0.0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6]
    intervals += [0.7, 0.75, 0.8, 0.85, 0.9, 0.95, 1.0]

    # Compute times required and pretty print
    header = "+------+---------+"
    print(header)
    print("|  [%] | time[s] |")
    print(header)
    for inter in intervals:
        print(
            "| {0:4d} | {1:7.2f} |".format(
                int(inter * 100), times[int((len(times) - 1) * inter)]
            )
        )
    print(header)


def compute_stats_compare(
    res1: List[ProblemAttempt], res2: List[ProblemAttempt], tag1: str, tag2: str
):
    # Function for comparing two experiments given their evaluation attempts

    # Extract the solved problem names of each approach
    solved1 = {p.name for p in res1 if p.solved}
    solved2 = {p.name for p in res2 if p.solved}

    # Get the problems solved in the intersection
    solved_union = solved1.union(solved2)
    print(f"Number of problems solved in the union: {len(solved_union)}")

    # Compute solved in intersection
    solved_intersection = solved1.intersection(solved2)
    print(f"Number of problems solved in the intersection: {len(solved_intersection)}")

    # Compute solved complements
    compl1 = solved1.difference(solved2)
    print(f"Number of problems solved only by {tag1}: {len(compl1)}")
    compl2 = solved2.difference(solved1)
    print(f"Number of problems solved only by {tag2}: {len(compl2)}")

    # Compute average runtime in the intersection
    if len(solved_intersection) > 0:
        avg1 = np.mean([p.runtime for p in res1 if p.name in solved_intersection])
        print(f"Avg solving time intersection {tag1}: {avg1:.2f}")

        avg2 = np.mean([p.runtime for p in res2 if p.name in solved_intersection])
        print(f"Avg solving time intersection {tag2}: {avg2:.2f}")
