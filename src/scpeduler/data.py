"""
Module for loading and processing evaluation data from a csv file.
"""
import sys
import logging
from typing import Optional, List, Set

import numpy as np
import pandas as pd

from scpeduler.stats import compute_greedy_min_cover

log = logging.getLogger()


def load_csv_file_as_df(csv_path: str) -> pd.DataFrame:
    # Create dataframe from csv, col-0 should be the problem indexes.
    df = pd.read_csv(csv_path, index_col=0)

    return df


def validate_evaluation_df(df: pd.DataFrame) -> None:
    # Validation flag
    valid = True

    if len(df.columns) < 1:
        valid = False
        log.warning("No heuristics found in the evaluation data. Please check format")

    if len(df.index) != len(set(df.index)):
        valid = False
        log.warning("The problems in the evaluation data are not unique.")

    if len(df.index) < 1:
        valid = False
        log.warning("No problems found in the evaluation data. Please check format")

    if not valid:
        log.error("Evaluation data files was invalid. Exiting ...")
        sys.exit(1)


def load_and_process_evaluation_data(
    data_path: str,
    runtime_lower_bound: Optional[float],
    runtime_upper_bound: Optional[float],
    keep_unsound_heuristics: bool,
    exclude_heuristics: Optional[List[str]],
    remove_dominated: bool,
    adm_sc: float,
    adm_sf: float,
) -> pd.DataFrame:
    # Load the CSV file as a DataFrame
    df = load_csv_file_as_df(data_path)
    print(
        f"Loaded evaluation data with {len(df.columns)} heuristics and {len(df.index)} problems"
    )

    # Quick validation
    validate_evaluation_df(df)

    # Remove any heuristics to exclude
    if exclude_heuristics is not None:
        df = remove_heuristics_from_df(df, exclude_heuristics)

    # Remove unsound heuristics
    if not keep_unsound_heuristics:
        df = remove_unsound_heuristics_from_df(df)

    # Mask runtimes if needed
    if runtime_lower_bound is not None or runtime_upper_bound is not None:
        df = mask_heuristic_runtimes(df, runtime_lower_bound, runtime_upper_bound)

    # Try to reduce the number of heuristics
    if remove_dominated:
        print("Removing dominated (redundant) heuristics according to admissibility")
        df = remove_dominated_heuristics(df, adm_sc, adm_sf)

    return df


def remove_heuristics_from_df(df: pd.DataFrame, heuristics: List[str]):
    df = df.drop(heuristics, axis=1, errors="ignore")
    if len(set(heuristics) - set(df.columns)) > 0:
        log.warning(
            f"Some heuristic not in data and could not be removed: {set(heuristics) - set(df.columns)}"
        )
    return df


def remove_unsound_heuristics_from_df(df: pd.DataFrame) -> pd.DataFrame:
    # Check if data contains an unsound result
    if -2 not in df.values:
        return df

    # Find columns with any unsound entry - returns all series with heur:bool
    is_unsound = (df == -2).any(axis=0)

    # Remove all unsound heuristics
    df = df[is_unsound[~is_unsound].index]
    if len(df.columns) == 0:
        log.error("All heuristics are unsound (-2)! Exiting ...")
        sys.exit(2)

    print(
        f"Removed {len(is_unsound[~is_unsound])} unsound heuristics. New number of heuristics: {len(df.columns)}"
    )
    return df


def mask_heuristic_runtimes(
    df: pd.DataFrame, lower_bound: Optional[float], upper_bound: Optional[float]
) -> pd.DataFrame:
    # Mask low timelimit
    if lower_bound is not None:
        df = df.mask(df < lower_bound, -1)

    # Mask high timelimit
    if upper_bound is not None:
        df = df.mask(df > upper_bound, -1)

    return df


def remove_unsolved_problems(data: pd.DataFrame) -> pd.DataFrame:
    res = data.loc[(data >= 0).any(axis=1)]
    return res


def get_test_data(training_data: pd.DataFrame, test_data_file: str) -> pd.DataFrame:
    # Load the evaluation data of the test file and assure that the files have matching heuristics
    # Load data file
    test_data = load_csv_file_as_df(test_data_file)
    validate_evaluation_df(test_data)
    print(f"Number of test problems: {len(test_data.index)}")
    problem_overlap = set(test_data.index).intersection(set(training_data.index))
    if len(problem_overlap) > 0:
        log.warning(
            f"Number of test problems occurring in the training data: {len(problem_overlap)}"
        )

    if not set(training_data.columns).issubset(set(test_data.columns)):
        log.error(
            "Test heuristics do not match training heuristics. Please check the data files."
        )
        log.error(f"Training: {','.join(training_data.columns)}")
        log.error(f"Testing: {','.join(test_data.columns)}")
        sys.exit(1)
    return test_data[training_data.columns]


def get_admissible_evaluation_matrix(df, adm_sc, adm_sf) -> pd.DataFrame:
    # Make the df safe for operations by replacing special values
    df = df.replace({-1: np.nan, -2: np.nan})

    # Get the fastest times
    fastest = df.min(axis=1)

    # Compute whether the heuristics are admissible
    df_adm = df.le(fastest + adm_sc, axis=0) | df.le(fastest * adm_sf, axis=0)
    return df_adm


def remove_dominated_heuristics(df, adm_sc, adm_sf) -> pd.DataFrame:
    # Compute the admissible evaluation matrix
    df_adm = get_admissible_evaluation_matrix(df, adm_sc, adm_sf)
    # TODO Just mock and check the flow

    # Compute the admissible set cover
    cover, universe = compute_greedy_min_cover(df_adm.replace({True: 1, False: -1}))
    print(
        f"Number of heuristics in admissible cover: {len(cover)} size of universe: {universe}"
    )

    # If all heuristics are in the set cover return these heuristics
    if len(df_adm.columns) == len(cover):
        print("All heuristics are in the adm cover. No redundant heuristics")
        return df

    redundant_heuristics = compute_redundant_heuristics(df_adm, cover)
    print(f"Number of redundant heuristics: {len(set(redundant_heuristics))}")
    if len(redundant_heuristics) > 0:
        dominating_heuristics = df.columns.difference(redundant_heuristics)
        df = df[dominating_heuristics]
    print(f"Number of heuristics after filtering: {len(df.columns)}")
    return df


def compute_redundant_heuristics(
    df_adm: pd.DataFrame, dominating: List[str]
) -> List[str]:
    redundant: List[str] = []
    unchecked: Set[str] = set(df_adm.columns) - set(dominating)

    assert len(dominating) > 0

    # For every unchecked heuristic - check if it adds value for all dominating heuristics
    for red in unchecked:
        is_redundant: bool = False
        for dom in dominating:
            # Compute whether red is adm when dom is not. If not, the heuristic is redundant
            if not ((df_adm[red] ^ df_adm[dom]) & df_adm[red]).any():
                is_redundant = True
                # Skip to the next heuristic as it is redundant
                redundant.append(red)
                break
        # It is a dominating heuristic - add to the set
        if not is_redundant:
            dominating.append(red)

        # All heuristics should be accounted for
    # print(f"{len(df_adm.columns)} {len(dominating)} {len(redundant)}")
    assert len(df_adm.columns) == len(dominating) + len(redundant)

    return redundant
