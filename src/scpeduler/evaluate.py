"""
Module for evaluating schedules.
"""
from pathlib import Path
from typing import List, Tuple
import json
import os
import pandas as pd
import logging

from scpeduler.analyse import compute_performance_stats
from scpeduler.data import get_test_data
from scpeduler.problem import get_problem_set, ProblemAttempt
from scpeduler.schedule import Schedule
from scpeduler.stats import (
    compute_solved_problems_upper_bound,
    compute_best_heuristic,
)

log = logging.getLogger()

EVALUATE_OUT_PATH = "output"


def evaluate_schedule(
    schedule: Schedule,
    evaluation_data: pd.DataFrame,
) -> List[ProblemAttempt]:
    # Get problems and evaluate
    problems = get_problem_set(evaluation_data)
    attempts = schedule.attempt_problems(problems)
    return attempts


def run_evaluation(
    solver_schedule: Schedule,
    data: pd.DataFrame,
    global_timeout: int,
    test_data_file: str,
    data_file: str,
    save_json_result: bool,
) -> None:
    # Construct single schedule on the top heuristic for performance comparison
    best_heuristic = compute_best_heuristic(data)
    single_schedule = Schedule(
        {"0": {best_heuristic[0]: global_timeout}}, global_timeout
    )
    print(f"Best heuristic: {best_heuristic[0]} Solves: {len(best_heuristic[1])}")

    print("\n\n# Training Evaluation #")
    solved_upper_bound = compute_solved_problems_upper_bound(data, global_timeout)
    print(f"Solved upper bound with all heuristics: {solved_upper_bound}")

    # Compute problem attempts
    solver_attempt_train, single_attempt_train = evaluate_on_dataset(
        solver_schedule, single_schedule, data
    )
    if save_json_result:
        solver_res_path = create_json_attempt_result_path(
            data_file, global_timeout, "solver_schedule"
        )
        save_attempts_as_json(solver_res_path, solver_attempt_train, "solver_schedule")
        solver_res_path = create_json_attempt_result_path(
            data_file, global_timeout, "single"
        )
        save_attempts_as_json(solver_res_path, single_attempt_train, "single")

    # Evaluate on test data if provided
    if test_data_file is not None:
        print("\n\n## Test Evaluation ##")
        test_data = get_test_data(data, test_data_file)
        evaluate_on_dataset(solver_schedule, single_schedule, test_data)


def evaluate_on_dataset(
    solver_schedule, single_schedule, data
) -> Tuple[List[ProblemAttempt], List[ProblemAttempt]]:
    solver_attempt_train = evaluate_schedule(solver_schedule, data)
    single_attempt_train = evaluate_schedule(single_schedule, data)

    # Evaluate and compare the performance on the train data
    compute_performance_stats(solver_attempt_train, single_attempt_train)
    return solver_attempt_train, single_attempt_train


def create_json_attempt_result_path(
    data_file: str, global_timeout: int, name_tag: str
) -> str:
    # Ensure the out path is created if not exists
    if not os.path.exists(EVALUATE_OUT_PATH):
        os.makedirs(EVALUATE_OUT_PATH)

    # Use the training file as the base name
    base_file = Path(data_file).stem
    save_path = f"{EVALUATE_OUT_PATH}/{base_file}_{name_tag}_{global_timeout}.json"
    return save_path


def save_attempts_as_json(
    file_path: str, attempts: List[ProblemAttempt], program_name: str
) -> None:
    # Remove the heuristic that solves the problem
    pos_results = [(p.name, p.runtime) for p in attempts if p.solved]

    stats = {prob: {"status": True, "rtime": float(t)} for prob, t in pos_results}

    preamble = {"program": program_name, "prog_alias": program_name}
    solver_res = {"stats": stats, "preamble": preamble}

    with open(file_path, "w") as outfile:
        json.dump(solver_res, outfile)
    print("Saved json result in: ", file_path)
