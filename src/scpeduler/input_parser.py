"""
Module for creating and storing input parsers associated with SCPeduler.
"""
import argparse
import logging

from typing import List


def get_compute_input_args(args: List[str]) -> argparse.Namespace:
    parser = argparse.ArgumentParser()

    # Positional arguments
    parser.add_argument(
        "evaluation_data",
        help="Path to the csv data file containing the evaluation data",
    )
    parser.add_argument(
        "global_timeout", type=int, help="Timeout for the total running time (cpu)"
    )

    # Optional arguments
    parser.add_argument(
        "--no_schedule_cores",
        type=int,
        default=1,
        help="The number of schedules running in parallel",
    )

    parser.add_argument(
        "--keep_unsound_heuristics",
        action="store_true",
        help="Do not filter unsound heuristics (-2) from the evaluation data",
    )

    parser.add_argument(
        "--remove_dominated_heuristics",
        action="store_true",
        help="Remove heuristics that do not add any value with respect to admissibility",
    )

    parser.add_argument(
        "--report_evaluation_data_stats",
        action="store_true",
        help="Compute and report key statistics of the provided evaluation data",
    )

    parser.add_argument(
        "--max_heuristic_runtime",
        type=int,
        default=None,
        help="Max running time for a single heuristic. Default: global_timeout",
    )

    parser.add_argument(
        "--mask_lower", type=int, default=None, help="Runtime masking of lower bound"
    )

    parser.add_argument(
        "--exclude_heuristics",
        nargs="+",
        help="Heuristics to exclude from the data",
        default=None,
    )

    parser.add_argument(
        "--save_json_result",
        action="store_true",
        help="Output solved runtime data to use for plotting",
    )

    # Cp solver arguments
    parser.add_argument(
        "--solver_workers", type=int, default=4, help="Number of workers for the solver"
    )

    parser.add_argument(
        "--solver_max_time",
        type=float,
        default=float("inf"),
        help="Maximum runtime for solver in seconds. Solver returns current best solution upon timeout (if found)",
    )

    # Admissible argument
    parser.add_argument(
        "--admissible",
        action="store_true",
        help="Compute local schedules via admissible clustering",
    )
    parser.add_argument(
        "--admissible_filter_rate",
        type=float,
        default=None,
        help="""At what consuming admissible rate to filter problems. None is no filtering. Range is {0.0, 1.0}""",
    )
    parser.add_argument(
        "--admissible_filter_keep",
        action="store_true",
        help="If set, return filtered problems as separate cluster",
    )
    parser.add_argument(
        "--admissible_sc",
        type=float,
        default=2.0,
        help="The admissible slack constant (also used for removing weak heuristics)",
    )
    parser.add_argument(
        "--admissible_sf",
        type=float,
        default=1.15,
        help="The admissible slack factor (also used for removing weak heuristics)",
    )
    parser.add_argument(
        "--admissible_no_clusters",
        type=int,
        default=None,
        help="Set the number of desired clusters instead of computing the optimal K value",
    )

    # Data file containing evaluation data
    parser.add_argument(
        "--test_data_file",
        help="Evaluation file used to test scheduler. Same heuristics, different problem samples",
    )

    parser.add_argument(
        "--print_deploy_string",
        action="store_true",
        help="Print schedule in casc driver script format",
    )

    # Set log levels
    parser.add_argument(
        "-d",
        "--debug",
        help="Print debug info",
        action="store_const",
        dest="loglevel",
        const=logging.DEBUG,
        default=logging.INFO,
    )
    parser.add_argument(
        "-q",
        "--quiet",
        help="Be quiet",
        action="store_const",
        dest="loglevel",
        const=logging.WARNING,
    )

    return parser.parse_args(args=args)


def get_download_input_args(args: List[str]) -> argparse.Namespace:
    parser = argparse.ArgumentParser()

    parser.add_argument(
        "heuristic_set",
        nargs="+",
        help="Heuristic set(s) (defined in download_data_config.py) to download from the database",
    )

    parser.add_argument(
        "--ltb",
        action="store_true",
        help="Considers the problems as LTB (UNSAT Success only)",
    )

    return parser.parse_args(args=args)
