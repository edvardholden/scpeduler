"""
Module for containing classes concerning problems and problem attempts.
"""
from typing import Dict, Tuple, Optional, List
import logging

import pandas as pd


log = logging.getLogger()


class Problem:
    def __init__(self, name: str, evaluation: Dict[str, int], embedding=None):
        self.name = name
        self.evaluation = evaluation
        self.embedding = embedding

        if self.embedding is not None:
            log.warning("Embedding temporarily not supported")

    def get_name(self) -> str:
        return self.name

    def evaluate(self, heuristic: str, time: int) -> Tuple[bool, int]:
        runtime = self.evaluation[heuristic]
        if runtime == -1 or runtime > time:
            # Problem not solved by the resources
            return False, time
        else:
            return True, runtime

    def __str__(self) -> str:
        return self.name


class ProblemAttempt:
    def __init__(
        self,
        problem: Problem,
        solved: bool,
        runtime: float | int,
        solved_by: Optional[str],
    ):
        self.problem = problem
        self.name = problem.get_name()
        self.solved = solved
        self.runtime = runtime
        self.solved_by = solved_by
        self.schedule = None

    def set_schedule(self, schedule):
        self.schedule = schedule
        return self

    def __str__(self) -> str:
        return self.__repr__()

    def __repr__(self) -> str:
        return f"Problem:{self.problem.get_name()} Solved:{self.solved}"


def get_problem_set(data: pd.DataFrame) -> List[Problem]:
    # Get name and evaluation of each problem for ease of use during an attempt
    problems = []
    for prob in data.index:
        problem = Problem(prob, dict(data.loc[prob]))
        problems.append(problem)

    return problems
