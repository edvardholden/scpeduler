"""
Module for converting CP solutions to a schedule and operating with the schedule.
"""
import logging
from typing import TYPE_CHECKING, Dict, List, Tuple, Optional

from scpeduler.problem import ProblemAttempt, Problem

log = logging.getLogger()

if TYPE_CHECKING:
    from scpeduler.solver import _SOLVER_SOLUTION

_CORE_HEUR_TIME = Dict[str, int]
_CORE_SCHEDULE = List[Tuple[str, int]]
_SCHEDULE = List[List[Tuple[str, int]]]


def order_schedule(core_time_pairs: "_SOLVER_SOLUTION", timeout: int) -> _SCHEDULE:
    # Order the time pairs for each core and return as a list
    schedule = []
    for core in core_time_pairs.values():
        schedule.append(compute_order(core, timeout))
    return schedule


def remove_unallocated_heuristics(time_pairs: _CORE_HEUR_TIME) -> _CORE_HEUR_TIME:
    # Delete heuristics where the value is 0 (not ran)
    for k, v in list(time_pairs.items()):
        if v < 1:
            del time_pairs[k]

    return time_pairs


def compute_order(time_pairs: _CORE_HEUR_TIME, timeout: int) -> _CORE_SCHEDULE:
    time_pairs = remove_unallocated_heuristics(time_pairs)
    # Check if all items are deleted (no heuristics ran at all)
    if len(time_pairs) == 0:
        log.warning(
            "No heuristics ran in the generated core schedule. Possibly an issue with the evaluation data."
        )
        return []  # Returning empty schedule

    if len(time_pairs) == 1:
        # Only one heuristic to run
        return [(list(time_pairs)[0], timeout)]

    # Smallest times first, then longer and longer times
    heuristic_order = sorted(time_pairs.items(), key=lambda kv: (kv[1], kv[0]))
    return heuristic_order


class Schedule:
    # Schedule returned by the solver maybe?
    def __init__(self, core_time_pairs: "_SOLVER_SOLUTION", timeout: int):
        # Store the timeout
        self.timeout = timeout

        # Compute schedule
        self.schedule: _SCHEDULE = order_schedule(core_time_pairs, self.timeout)

    def attempt(self, problem: Problem):
        # Simulate the problem attempt on each core in parallel
        # Global flags of the current problem attempt
        solved: bool = False
        solved_by: Optional[str] = None
        wc_time: float = float(self.timeout)

        # Attempt the problem with the schedule of each core - need to run all to check for the shortest time
        for sched in self.schedule:
            # Run the schedule for this core
            core_time = 0
            for i, (heur, heur_time) in enumerate(sched):
                # If it is the last heuristic of the core, just run until timeout
                if i == len(sched) - 1:
                    heur_time = self.timeout

                # Evaluate the problem and add to the runtime
                core_solved, runtime = problem.evaluate(heur, heur_time)
                core_time += runtime

                # If the core solved the problem within the timeout
                if core_solved and self.timeout >= core_time:
                    # If another core was faster or prev unsolved, update with the best result
                    if wc_time >= core_time or not solved:
                        solved = True
                        wc_time = core_time
                        solved_by = heur

                    # Core solved the problem, move to the next core
                    break

        return ProblemAttempt(problem, solved, wc_time, solved_by)

    def attempt_problems(self, problems) -> List[ProblemAttempt]:
        attempts = []
        for problem in problems:
            attempts.append(self.attempt(problem))

        return attempts

    def __str__(self):
        return ", ".join(str(s) for s in self.schedule)


def print_schedule(schedule: Schedule, end=",\n") -> str:
    deploy = ""
    for core in schedule.schedule:
        deploy += (
            "["
            + ", ".join("('heur/" + str(s[0]) + "', " + str(s[1]) + ")" for s in core)
            + "]"
            + end
        )

    print(deploy)
    return deploy


def convert_solution_to_schedule(
    solver_solution: "_SOLVER_SOLUTION", global_timeout: int
) -> Schedule:
    schedule = Schedule(solver_solution, global_timeout)
    return schedule


def report_schedule(schedule: Schedule, print_deploy_string: bool) -> None:
    if print_deploy_string:
        print()
        print("Deployment String:")
        print_schedule(schedule)
    else:
        print()
        print(f"\nSchedule: {schedule}")
