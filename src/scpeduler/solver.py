"""
Module containing the encoding of the heuristic scheduling problem.
"""
from typing import Optional, List, Dict, Tuple
import numpy as np
import pandas as pd
from ortools.sat.python import cp_model
import logging

from scpeduler.data import remove_unsolved_problems

# Setup logger
log = logging.getLogger()

_SOLVER_SOLUTION = Dict[str, Dict[str, int]]


class Solver:
    def __init__(self, workers: int, solver_max_time: int):
        # Instantiate the cp solver
        self.solver = cp_model.CpSolver()
        self.solver.parameters.num_search_workers = workers
        self.solver.parameters.max_time_in_seconds = solver_max_time

        self.evaluation_heuristics: Optional[List[str]] = None
        self.evaluation_data: Optional[np.ndarray] = None

    def set_evaluation_data(self, data: pd.DataFrame):
        # Convert the data from df to numpy arrays
        self.evaluation_heuristics = list(data.columns)

        # Filter the problems that cannot be solved by any heuristic to reduce unnecessary constraints
        data = remove_unsolved_problems(data)

        # Convert to integers by ceiling
        data = data.apply(np.ceil)

        self.evaluation_data = data.values.astype(np.int64)
        return self

    def build_model(
        self, global_timeout: int, max_heuristic_runtime: Optional[int], no_cores: int
    ):
        # If max_heuristic_runtime is None, it is the same as wc timeout
        if max_heuristic_runtime is None:
            max_heuristic_runtime = global_timeout

        # Construct a model for the schedule over the data
        assert self.evaluation_heuristics is not None
        assert self.evaluation_data is not None

        log.info(
            f"Constructing model for {self.evaluation_heuristics} heuristics and {no_cores} cores "
            + f"on {len(self.evaluation_data[0])} problems"
        )

        # Build the model
        self.__build_cp_model_parallel_fast(
            self.evaluation_data,
            self.evaluation_heuristics,
            global_timeout,
            max_heuristic_runtime,
            no_cores,
        )
        return self

    def __build_cp_model_parallel_fast(
        self,
        data,
        heuristics: List[str],
        global_timeout: int,
        max_heuristic_runtime: int,
        no_cores: int,
    ):
        # Initialise the model
        model = cp_model.CpModel()
        no_heuristics = len(heuristics)
        no_problems = len(data)

        # To construct a schedule, we want to find out how long to run each heuristic for.
        # How long to run a heuristic is modelled through the variable heur_time, and the
        # list of these variables represents all the heuristics.
        # A heuristic can either run for any integer between 0 and wc_timeout, where 0 means that it doesn't run at all.
        heur_time: List[cp_model.IntVar] = []
        for heur_no in range(no_heuristics):
            heur_time += [
                model.NewIntVar(0, global_timeout, f"{heuristics[heur_no]}_time")
            ]

        # Set the max runtime of each heuristic
        for heur in heur_time:
            model.Add(heur <= max_heuristic_runtime)

        # Model the runtime of a heuristic on a core
        core_heur: Dict[Tuple[int, int], cp_model.IntVar] = {}
        for core_no in range(no_cores):
            for heur_no in range(no_heuristics):
                core_heur[(core_no, heur_no)] = model.NewIntVar(
                    0, global_timeout, f"core_heur_{core_no}_{heuristics[heur_no]}"
                )

        # The running time of a heuristic is the sum of its running time on a core
        for heur_no in range(no_heuristics):
            model.Add(
                heur_time[heur_no]
                == sum(core_heur[(core_no, heur_no)] for core_no in range(no_cores))
            )

        # A heuristic can only run on one core
        # Intermediate variable for representing when the heuristics are used (have a runtime greater than 0)
        core_heur_active: Dict[Tuple[int, int], cp_model.IntVar] = {}
        for core_no in range(no_cores):
            for heur_no in range(no_heuristics):
                core_heur_active[(core_no, heur_no)] = model.NewBoolVar(
                    f"A_{core_no}_{heuristics[heur_no]}"
                )
                model.Add(core_heur[(core_no, heur_no)] > 0).OnlyEnforceIf(
                    core_heur_active[(core_no, heur_no)]
                )
                model.Add(core_heur[(core_no, heur_no)] == 0).OnlyEnforceIf(
                    core_heur_active[(core_no, heur_no)].Not()
                )

        for heur_no in range(no_heuristics):
            for core_no in range(no_cores):
                model.Add(
                    sum(
                        core_heur[(other_core, heur_no)]
                        for other_core in range(no_cores)
                        if other_core != core_no
                    )
                    == 0
                ).OnlyEnforceIf(core_heur_active[(core_no, heur_no)])

        #  The runtimes of the heuristics on a core are less than WC
        for core_no in range(no_cores):
            model.Add(
                sum(core_heur[(core_no, heur)] for heur in range(no_heuristics))
                <= global_timeout
            )

        # We need to model the relationship between every problem-heuristic pair to model
        # whether the pair yields a solution, and in what time.
        # The variable solved_pair models whether the problem-heuristic pair is solved in the assignment.
        solved_pair: Dict[Tuple[int, int], cp_model.IntVar] = {}
        for heur_no in range(no_heuristics):
            for prob_no in range(no_problems):
                # Check dependency here
                solved_pair[(prob_no, heur_no)] = model.NewBoolVar(
                    f"solved_pair_{prob_no}_{heur_no}"
                )

        # Link the problem-heuristic pairs to the evaluation/runtime data provided in data
        for heur_no in range(no_heuristics):
            for prob_no in range(no_problems):
                # If the problem-heuristic pair is solved
                if data[prob_no][heur_no] != 0 and data[prob_no][heur_no] != -1:
                    # Model that the problem-heuristic pair is solved if the heuristic value is sufficiently large.
                    # The pair is not solved if the heuristic value is less than the required runtime
                    model.Add(
                        heur_time[heur_no] < data[prob_no][heur_no]
                    ).OnlyEnforceIf(solved_pair[(prob_no, heur_no)].Not())
                    # The pair is solved if the heuristic value is equal or greater than
                    # the required runtime
                    model.Add(
                        heur_time[heur_no] >= data[prob_no][heur_no]
                    ).OnlyEnforceIf(solved_pair[(prob_no, heur_no)])

                else:
                    # The pair is never solved for any known heuristic value, hence we make sure it can never be true.
                    # The pair is not solved when the heuristic value is greater than 0
                    model.Add(heur_time[heur_no] >= 0).OnlyEnforceIf(
                        solved_pair[(prob_no, heur_no)].Not()
                    )
                    # The pair is solved if the heuristic value is less than 0 (which is
                    # not in the domain)
                    model.Add(heur_time[heur_no] < 0).OnlyEnforceIf(
                        solved_pair[(prob_no, heur_no)]
                    )

        # Create a variable which models weather a problem is solved by any heuristic.
        solved_problem: List[cp_model.IntVar] = []
        for n in range(no_problems):
            solved_problem += [model.NewBoolVar(f"solved_problem_{n}")]

        # Link the solved problem variable to the problem-heuristic pair variables.
        # A problem is solved by the schedule if at least one heuristic is assigned
        # a value which is large enough to solve it.
        for prob_no in range(no_problems):
            # The Problem is solved if at least one pair solves it
            model.Add(
                sum(solved_pair[(prob_no, heur_no)] for heur_no in range(no_heuristics))
                >= 1
            ).OnlyEnforceIf(solved_problem[prob_no])
            # A problem is not solved if no pair solves it
            model.Add(
                sum(solved_pair[(prob_no, heur_no)] for heur_no in range(no_heuristics))
                < 1
            ).OnlyEnforceIf(solved_problem[prob_no].Not())

        # The solver is supposed to find the heuristic runtime assignment which optimises the number of solved problems.
        # Hence, we provide the sum of solved problems as the maximization objective
        model.Maximize(sum(solved_problem))

        # Report the model stats if in debug mode
        log.debug(model.ModelStats())

        # Create dict of var models for debugging
        model_vars = {
            "heur_time": heur_time,
            "solved_problem": solved_problem,
            "solved_pair": solved_pair.values(),
            "core_heur": core_heur.values(),
        }

        # Update solver
        self.model = model
        self.model_heuristics = heuristics
        self.model_vars = model_vars
        self.no_cores = no_cores

        return self

    def run_cp_solver(self) -> None:
        # Report start
        log.info("Started solver ..")

        # Run the solver and get the status
        status = self.solver.Solve(self.model)

        report = "Solver finished!\n"
        report += f"Solver status: {self.solver.StatusName(status)}\n"
        report += f"Optimal objective value: {self.solver.ObjectiveValue()}\n\n"
        report += "Statistics\n"
        report += f"  - wall time : {self.solver.WallTime()} s"
        log.info(report)

        # Report more detail
        log.debug(f"  - conflicts : {self.solver.NumConflicts()}")
        log.debug(f"  - branches  : {self.solver.NumBranches()}")

    def compute_schedule(self) -> _SOLVER_SOLUTION:
        # Run solver on the model
        self.run_cp_solver()

        # Report variables if debugging
        if log.level <= logging.DEBUG:
            self.__report_solver_vars()

        # Extract solution
        result = self.get_solver_result()

        # Return the result
        return result

    def get_solver_result(self) -> _SOLVER_SOLUTION:
        # Get the heuristic running time of each core

        # Get the heuristic runtimes
        res_times: Dict[str, int] = {}
        for heur in self.model_vars["heur_time"]:
            # Map the result to the heuristic
            res_times[heur.Name().replace("_time", "", 1)] = self.solver.Value(heur)

        # Initialise core assignment
        res_cores: Dict[str, Dict[str, int]] = {}
        for core in range(self.no_cores):
            res_cores[str(core)] = {}

        # Extract the heuristic assignment of each core
        for core_heur in self.model_vars["core_heur"]:
            if self.solver.Value(core_heur):
                # Extract the core and heuristic name
                core_heur_tag = core_heur.Name().replace("core_heur_", "", 1)
                core_tag, heur_tag = core_heur_tag.split("_", 1)
                res_cores[core_tag][heur_tag] = res_times[heur_tag]

        # Remove any unused cores
        for k in list(res_cores.keys()):
            if len(res_cores[k]) == 0:
                del res_cores[k]

        if len(res_cores) < self.no_cores:
            log.warning("Warning: Schedule is not utilising all the cores")

        return res_cores

    def __report_solver_vars(self) -> None:
        log.debug("### DEBUG MODEL VARS ###")
        for var_type, var_list in self.model_vars.items():
            log.debug(f"### VARS: {var_type}")
            for var in var_list:
                log.debug(f"{var.Name()} = {self.solver.Value(var)}")


def solve_scheduling_problem(
    solver_workers: int,
    solver_max_time: int,
    evaluation_data: pd.DataFrame,
    global_timeout: int,
    max_heuristic_runtime: int,
    no_cores: int,
) -> _SOLVER_SOLUTION:
    # Solve the scheduling problem over the given evaluation
    solver = Solver(solver_workers, solver_max_time)
    solver.set_evaluation_data(evaluation_data)
    solver.build_model(global_timeout, max_heuristic_runtime, no_cores)

    solution = solver.compute_schedule()
    return solution
