"""
Module for computing basic statistics oover the evalaution data.
"""
from typing import List, Dict, Set, Tuple, Optional

import pandas as pd


def compute_top_n_heuristics(data: pd.DataFrame, n: int) -> List[Tuple[str, Set[str]]]:
    # Compute solved per heuristic
    heuristic_solved = compute_solved_problems_heuristic(data)
    # Order by most solved
    top_heuristics = sorted(heuristic_solved.items(), key=lambda x: -len(x[1]))
    # Return top n
    return top_heuristics[:n]


def compute_solved_problems_heuristic(data: pd.DataFrame) -> Dict[str, Set[str]]:
    solved = {}
    for heur in data.columns:
        solved[heur] = set(data.loc[data[heur] >= 0].index)
    return solved


def compute_greedy_min_cover(data: pd.DataFrame) -> Tuple[List[str], int]:
    solved_sets = compute_solved_problems_heuristic(data)
    universe = compute_universe(solved_sets)
    cover, _ = compute_set_cover(universe, solved_sets)

    return cover, len(universe)


def compute_universe(heuristic_set: Dict[str, Set[str]]) -> Set[str]:
    universe: Set[str] = set()
    for _, v in heuristic_set.items():
        universe = universe.union(v)
    return universe


def compute_set_cover(
    universe: Set[str], subsets: Dict[str, Set[str]]
) -> Tuple[List[str], List[int]]:
    covered: Set[str] = set()
    cover = []
    cover_vals = []

    while covered != universe:
        max_cover: Optional[str] = None
        max_cover_val: int = 0
        for k, v in subsets.items():
            if len(v - covered) > max_cover_val:
                max_cover_val = len(v - covered)
                max_cover = k

        assert max_cover is not None
        covered = covered.union(subsets[max_cover])
        del subsets[max_cover]

        cover += [max_cover]
        cover_vals += [max_cover_val]

    return cover, cover_vals


def compute_solved_problems_upper_bound(data: pd.DataFrame, timeout: int) -> int:
    return sum(((data >= 0) & (data <= timeout)).any(axis=1))


def compute_total_solving_time(data: pd.DataFrame) -> pd.DataFrame:
    return data[data >= 0].sum()


def compute_best_heuristic(data: pd.DataFrame) -> Tuple[str, Set[str]]:
    # Get a solution by all heuristics
    heuristic_solved = compute_solved_problems_heuristic(data)

    # Get the top solving heuristics
    max_solved = max(map(len, heuristic_solved.values()))
    best_heuristics = [
        heur
        for heur, no_solved in heuristic_solved.items()
        if max_solved == len(no_solved)
    ]

    # If only one top solver - return it
    if len(best_heuristics) == 1:
        best_heuristic = best_heuristics[0]
        return best_heuristic, heuristic_solved[best_heuristic]

    # Multiple heuristics have the same number of solutions - discriminate on minimum solving time
    # Compute solving times on these heuristics and get the minimum
    best_times = compute_total_solving_time(data[best_heuristics])
    print(best_times)
    best_heuristic = str(best_times.idxmin(axis=0))
    return best_heuristic, heuristic_solved[best_heuristic]
