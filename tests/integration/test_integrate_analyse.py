from scpeduler.analyse import report_evaluation_data_stats
from scpeduler.data import load_csv_file_as_df


def test_report_evaluation_data_stats():
    eval_data = load_csv_file_as_df("tests/res/data/small_data.csv")
    # Check that it runs without issues
    report_evaluation_data_stats(eval_data)
