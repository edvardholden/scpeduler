from scpeduler.data import load_csv_file_as_df
from scpeduler.evaluate import run_evaluation
from scpeduler.schedule import Schedule


def test_run_evaluation():
    solution = {"0": {"H1": 2}, "1": {"H2": 10, "H3": 3}}
    eval_data = load_csv_file_as_df("tests/res/data/small_data.csv")
    schedule = Schedule(solution, 15)
    # Check that it runs without issues
    run_evaluation(
        schedule,
        eval_data,
        15,
        "tests/res/data/small_data.csv",
        "tests/res/data/small_data.csv",
        False,
    )
