from scpeduler.solver import solve_scheduling_problem
from scpeduler.data import load_csv_file_as_df


def test_solve_scheduling_problem_single_core():
    eval_data = load_csv_file_as_df("tests/res/data/small_data.csv")
    res = solve_scheduling_problem(1, 20, eval_data, 15, 15, 1)
    assert res == {"0": {"H1": 2, "H3": 10}}


def test_solve_scheduling_problem_double_core():
    eval_data = load_csv_file_as_df("tests/res/data/small_data.csv")
    res = solve_scheduling_problem(1, 20, eval_data, 15, 15, 2)
    assert res == {"0": {"H1": 2}, "1": {"H2": 10, "H3": 3}}
