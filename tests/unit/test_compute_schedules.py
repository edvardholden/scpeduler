import logging

from mock import mock
from compute_schedules import main


@mock.patch("compute_schedules.run_evaluation")
@mock.patch("compute_schedules.report_schedule")
@mock.patch("compute_schedules.convert_solution_to_schedule")
@mock.patch("compute_schedules.solve_scheduling_problem")
@mock.patch("compute_schedules.report_evaluation_data_stats")
@mock.patch("compute_schedules.load_and_process_evaluation_data")
@mock.patch("compute_schedules.get_compute_input_args", autospec=True)
def test_main(
    mock_parser,
    mock_load_data,
    mock_report_stats,
    mock_solve_schedule,
    mock_convert,
    mock_report_sched,
    mock_run_eval,
):
    mock_parser.return_value.loglevel = logging.INFO
    mock_parser.return_value.max_heuristic_runtime = None
    mock_parser.return_value.global_timeout = 300

    main()

    mock_parser.assert_called_once()
    assert mock_parser.return_value.max_heuristic_runtime == 300
    mock_load_data.assert_called_once()
    mock_report_stats.assert_called_once()
    mock_solve_schedule.assert_called_once()
    mock_convert.assert_called_once()
    mock_report_sched.assert_called_once()
    mock_run_eval.assert_called_once()
