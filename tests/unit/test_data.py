import mock
import pytest

import pandas as pd

from scpeduler.data import (
    load_csv_file_as_df,
    validate_evaluation_df,
    remove_heuristics_from_df,
    remove_unsound_heuristics_from_df,
    mask_heuristic_runtimes,
    load_and_process_evaluation_data,
    get_test_data,
    get_admissible_evaluation_matrix,
    remove_dominated_heuristics,
    compute_redundant_heuristics,
)


def get_small_df():
    return pd.DataFrame(
        {"h1": [1, -1, 3, 4], "h2": [6, 6, 8, -1], "h3": [9, 10, 12, 12]}
    )


def test_load_csv_file_as_df_success():
    df = load_csv_file_as_df("tests/res/data/small_data.csv")

    assert list(df.columns) == ["H1", "H2", "H3"]
    assert list(df.index) == ["p1", "p2", "p3", "p4", "p5", "p6"]


def test_validate_evaluation_df_success():
    df = load_csv_file_as_df("tests/res/data/small_data.csv")
    validate_evaluation_df(df)


def test_validate_evaluation_df_no_evaluation_data(caplog):
    df = load_csv_file_as_df("tests/res/data/empty_data.csv")

    with pytest.raises(SystemExit):
        validate_evaluation_df(df)

    assert caplog.text != ""


def test_validate_evaluation_df_no_problems(caplog):
    df = load_csv_file_as_df("tests/res/data/no_problems_data.csv")

    with pytest.raises(SystemExit):
        validate_evaluation_df(df)

    assert caplog.text != ""


def test_validate_evaluation_df_no_data_duplicate_indexes(caplog):
    df = load_csv_file_as_df("tests/res/data/duplicate_index_data.csv")

    with pytest.raises(SystemExit):
        validate_evaluation_df(df)

    assert caplog.text != ""


def test_remove_heuristics_from_df_none():
    df = load_csv_file_as_df("tests/res/data/small_data.csv")
    res = remove_heuristics_from_df(df, [])
    assert df.equals(res)


def test_remove_heuristics_from_df_single():
    df = load_csv_file_as_df("tests/res/data/small_data.csv")
    res = remove_heuristics_from_df(df, list(df.columns[:1]))
    assert list(df.columns[1:]) == list(res.columns)


def test_remove_heuristics_from_df_all():
    df = load_csv_file_as_df("tests/res/data/small_data.csv")
    res = remove_heuristics_from_df(df, list(df.columns))
    assert list(res.columns) == []


def test_exclude_heuristics_single_not_in_data(caplog):
    df = load_csv_file_as_df("tests/res/data/small_data.csv")
    res = remove_heuristics_from_df(df, list(df.columns[:1]) + ["NOT EXIST"])
    assert list(df.columns[1:]) == list(res.columns)
    assert "not be removed" in caplog.text


def test_remove_unsound_heuristics_from_df_no_unsound():
    df = pd.DataFrame({"h1": [1, 2, 3, -1], "h2": [5, 6, 7, 8], "h3": [9, 10, 11, 12]})
    res = remove_unsound_heuristics_from_df(df)
    assert df.equals(res)


def test_remove_unsound_heuristics_from_df_single_unsound(caplog):
    df = pd.DataFrame({"h1": [1, 2, 3, -1], "h2": [-2, 6, 7, 8], "h3": [9, 10, 11, 12]})
    res = remove_unsound_heuristics_from_df(df)
    assert list(res.columns) == ["h1", "h3"]


def test_remove_unsound_heuristics_from_df_all(caplog):
    df = pd.DataFrame(
        {"h1": [1, 2, -1, -2], "h2": [-2, 6, 7, 8], "h3": [-2, -2, -2, -2]}
    )
    with pytest.raises(SystemExit):
        remove_unsound_heuristics_from_df(df)

    assert "are unsound" in caplog.text


def test_mask_heuristic_runtimes_no_masking():
    df = get_small_df()
    res = mask_heuristic_runtimes(df, None, None)
    assert df.equals(res)


def test_mask_heuristic_runtimes_mask_lower():
    df = get_small_df()
    exp = pd.DataFrame(
        {"h1": [-1, -1, -1, -1], "h2": [-1, -1, 8, -1], "h3": [9, 10, 12, 12]}
    )
    res = mask_heuristic_runtimes(df, 7, None)
    assert exp.equals(res)


def test_mask_heuristic_runtimes_mask_higher():
    df = get_small_df()
    exp = pd.DataFrame(
        {"h1": [1, -1, 3, 4], "h2": [6, 6, -1, -1], "h3": [-1, -1, -1, -1]}
    )
    res = mask_heuristic_runtimes(df, None, 7)
    assert res.equals(exp)


def test_mask_heuristic_runtimes_mask_both():
    df = get_small_df()
    exp = pd.DataFrame(
        {"h1": [-1, -1, -1, 4], "h2": [6, 6, 8, -1], "h3": [9, -1, -1, -1]}
    )
    res = mask_heuristic_runtimes(df, 4, 9)
    assert exp.equals(res)


@mock.patch("scpeduler.data.remove_dominated_heuristics")
@mock.patch("scpeduler.data.mask_heuristic_runtimes")
@mock.patch("scpeduler.data.remove_heuristics_from_df")
@mock.patch("scpeduler.data.validate_evaluation_df")
@mock.patch("scpeduler.data.load_csv_file_as_df")
def test_load_and_process_evaluation_data_minimal(
    mock_load, mock_val, mock_exclude, mock_mask, mock_dom
):
    mock_df = mock.MagicMock(pd.DataFrame())
    mock_load.return_value = mock_df
    df = load_and_process_evaluation_data(
        "data.csv", None, None, True, None, False, 0.0, 0.0
    )
    assert df is mock_df
    mock_load.assert_called_once_with("data.csv")
    mock_val.assert_called_once_with(mock_df)
    mock_exclude.assert_not_called()
    mock_mask.assert_not_called()
    mock_dom.assert_not_called()


@mock.patch("scpeduler.data.remove_dominated_heuristics")
@mock.patch("scpeduler.data.mask_heuristic_runtimes")
@mock.patch("scpeduler.data.remove_heuristics_from_df")
@mock.patch("scpeduler.data.validate_evaluation_df")
@mock.patch("scpeduler.data.load_csv_file_as_df")
def test_load_and_process_evaluation_data_maximal(
    mock_load, mock_val, mock_exclude, mock_mask, mock_dom
):
    mock_df = mock.MagicMock(pd.DataFrame())
    mock_load.return_value = mock_df
    df = load_and_process_evaluation_data(
        "data.csv", 2, 30, False, ["H1"], True, 2, 1.15
    )
    assert df is not None
    mock_load.assert_called_once_with("data.csv")
    mock_val.assert_called_once_with(mock_df)
    mock_exclude.assert_called_once_with(mock_df, ["H1"])
    mock_mask.assert_called_once_with(mock.ANY, 2, 30)
    mock_dom.assert_called_once_with(mock.ANY, 2, 1.15)


@mock.patch("scpeduler.data.validate_evaluation_df")
@mock.patch("scpeduler.data.load_csv_file_as_df")
def test_get_test_data(mock_load, mock_val, caplog):
    train_data = pd.DataFrame(
        {"h1": [-1, -1, 4], "h2": [6, 8, -1], "h3": [9, -1, -1]},
        index=["p1", "p2", "p3"],
    )
    test_data = pd.DataFrame(
        {"h1": [-1, -1, 4], "h2": [6, 8, -1], "h3": [9, -1, -1]},
        index=["p4", "p5", "p6"],
    )
    mock_load.return_value = test_data
    res = get_test_data(train_data, "test.csv")

    mock_load.assert_called_once_with("test.csv")
    mock_val.assert_called_once_with(test_data)
    assert caplog.text == ""
    assert res.equals(test_data)


@mock.patch("scpeduler.data.validate_evaluation_df")
@mock.patch("scpeduler.data.load_csv_file_as_df")
def test_get_test_data_overlap(mock_load, mock_val, caplog):
    train_data = pd.DataFrame(
        {"h1": [-1, -1, 4], "h2": [6, 8, -1], "h3": [9, -1, -1]},
        index=["p1", "p2", "p3"],
    )
    test_data = pd.DataFrame(
        {"h1": [-1, -1, 4], "h2": [6, 8, -1], "h3": [9, -1, -1], "h4": [2, 3, 4]},
        index=["p3", "p5", "p6"],
    )
    mock_load.return_value = test_data
    res = get_test_data(train_data, "test.csv")

    mock_load.assert_called_once_with("test.csv")
    mock_val.assert_called_once_with(test_data)
    assert "Number of test problems occurring in the training data: 1" in caplog.text
    assert res.equals(test_data[test_data.columns[:3]])


@mock.patch("scpeduler.data.validate_evaluation_df")
@mock.patch("scpeduler.data.load_csv_file_as_df")
def test_get_test_data_unmatch_heuristics(mock_load, mock_val, caplog):
    train_data = pd.DataFrame(
        {"h1": [-1, -1, 4], "h2": [6, 8, -1], "h3": [9, -1, -1]},
        index=["p1", "p2", "p3"],
    )
    test_data = pd.DataFrame(
        {"h30": [-1, -1, 4], "h80": [6, 8, -1], "h3": [9, -1, -1]},
        index=["p4", "p5", "p6"],
    )
    mock_load.return_value = test_data
    with pytest.raises(SystemExit):
        _ = get_test_data(train_data, "test.csv")

    mock_load.assert_called_once_with("test.csv")
    mock_val.assert_called_once_with(test_data)
    assert "heuristics do not match" in caplog.text


def test_get_admissible_evaluation_matrix():
    # Test all scenarios?
    # Three heuristics
    # unsolved, only one solution, only two admissible, factor adm, ratio adm, all adm
    df = pd.DataFrame(
        [
            [-1, -1, -1],
            [-1, -1, 0],
            [-1, 1, 1],
            [10, 2, 3],
            [-1, 100, 105],
            [1.5, 2, 23],
        ]
    )
    exp = pd.DataFrame(
        [[0, 0, 0], [0, 0, 1], [0, 1, 1], [0, 1, 1], [0, 1, 1], [1, 1, 0]]
    )
    res = get_admissible_evaluation_matrix(df, adm_sc=2, adm_sf=1.2)
    assert res.equals(exp == 1)


@mock.patch("scpeduler.data.compute_redundant_heuristics")
@mock.patch("scpeduler.data.compute_greedy_min_cover")
@mock.patch("scpeduler.data.get_admissible_evaluation_matrix")
def test_remove_dominated_heuristics_complete_cover(mock_adm, mock_cover, mock_red):
    mock_df = mock.Mock(pd.DataFrame)
    mock_cover.return_value = (range(10), mock.ANY)
    mock_df_adm = mock.Mock(pd.DataFrame)
    mock_df_adm.columns = range(10)
    mock_adm.return_value = mock_df_adm

    res = remove_dominated_heuristics(mock_df, 2, 1.2)

    mock_adm.assert_called_once_with(mock_df, 2, 1.2)
    mock_df_adm.replace.assert_called_once()
    mock_cover.assert_called_once()
    mock_red.asswer_not_called()
    assert res == mock_df


@mock.patch("scpeduler.data.compute_redundant_heuristics")
@mock.patch("scpeduler.data.compute_greedy_min_cover")
@mock.patch("scpeduler.data.get_admissible_evaluation_matrix")
def test_remove_dominated_heuristics_compute_red(mock_adm, mock_cover, mock_red):
    mock_df = mock.MagicMock(pd.DataFrame)
    mock_set_cover = mock.MagicMock(list)
    mock_set_cover.return_value = []
    mock_cover.return_value = (mock_set_cover, mock.ANY)
    mock_df_adm = mock.Mock(pd.DataFrame)
    mock_df_adm.columns = range(10)
    mock_adm.return_value = mock_df_adm

    remove_dominated_heuristics(mock_df, 2, 1.2)
    mock_adm.assert_called_once_with(mock_df, 2, 1.2)
    mock_df_adm.replace.assert_called_once()
    mock_cover.assert_called_once()
    mock_red.assert_called_once_with(mock_df_adm, mock_set_cover)


def test_compute_redundant_heuristics_empty_dom():
    heur = ["h1", "h2", "h3"]
    df = pd.DataFrame([[-1, -1, 10], [-1, 5, -1], [3, -1, -1]], columns=heur)
    with pytest.raises(AssertionError):
        _ = compute_redundant_heuristics(df, [])


def test_compute_redundant_heuristics_all_cover():
    heur = ["h1", "h2", "h3"]
    df = pd.DataFrame([[0, 0, 1], [0, 1, 0], [1, 0, 0]], columns=heur)
    res = compute_redundant_heuristics(df, ["h1"])
    assert res == []


def test_compute_redundant_heuristics_double():
    heur = ["h1", "h2", "h3"]
    df = pd.DataFrame([[1, 0, 0], [1, 1, 0], [1, 0, 0]], columns=heur)
    res = compute_redundant_heuristics(df, ["h1"])
    assert set(res) == {"h2", "h3"}


def test_compute_redundant_heuristics_single():
    heur = ["h1", "h2", "h3"]
    df = pd.DataFrame([[1, 0, 0], [1, 1, 0], [0, 1, 0]], columns=heur)
    res = compute_redundant_heuristics(df, ["h1"])
    assert res == ["h3"]
