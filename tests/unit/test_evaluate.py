import mock

from scpeduler.evaluate import (
    create_json_attempt_result_path,
    evaluate_on_dataset,
)


@mock.patch("scpeduler.evaluate.EVALUATE_OUT_PATH", "output")
def test_create_json_attempt_result_path():
    res = create_json_attempt_result_path("train_data.csv", 100, "solver")
    assert res == "output/train_data_solver_100.json"


@mock.patch("scpeduler.evaluate.compute_performance_stats")
@mock.patch("scpeduler.evaluate.evaluate_schedule")
def test_evaluate_on_dataset(mock_eval, mock_compute):
    mock_solver = mock.Mock()
    mock_single = mock.Mock()
    mock_data = mock.Mock()
    evaluate_on_dataset(mock_solver, mock_single, mock_data)

    assert mock_eval.call_count == 2
    mock_eval.assert_any_call(mock_solver, mock_data)
    mock_eval.assert_any_call(mock_single, mock_data)
    mock_compute.assert_called_once()
