from typing import List

import pytest

from scpeduler.input_parser import get_compute_input_args, get_download_input_args


@pytest.mark.parametrize("inp", [[], ["evaluation_data"], ["100"]])
def test_compute_missing_args(inp):
    with pytest.raises(SystemExit):
        get_compute_input_args(args=inp)


def test_compute_correct_args():
    inp = ["evaluation_data", "100"]
    args = get_compute_input_args(args=inp)
    assert args.evaluation_data == "evaluation_data"
    assert args.global_timeout == 100


def test_download_missing_args():
    inp: List[str] = []
    with pytest.raises(SystemExit):
        get_download_input_args(args=inp)


def test_download_args_single():
    inp = ["ueq_test"]
    args = get_download_input_args(args=inp)
    assert args.heuristic_set == inp
    assert not args.ltb


def test_download_args_multiple():
    inp = ["ueq_test", "fnt_large"]
    args = get_download_input_args(args=inp)
    assert args.heuristic_set == inp
    assert not args.ltb


def test_download_args_ltb():
    inp = ["ueq_test", "fnt_large", "--ltb"]
    args = get_download_input_args(args=inp)
    assert args.heuristic_set == inp[:2]
    assert args.ltb
