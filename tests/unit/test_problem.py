import pandas as pd
import pytest

from scpeduler.problem import Problem, ProblemAttempt, get_problem_set


def test_problem_init():
    name = "problem.p"
    evaluation = {"a": 7, "b": -1}
    p = Problem(name, evaluation)
    assert p.name == name
    assert p.evaluation == evaluation
    assert p.get_name() == name
    assert str(p) == name


def test_problem_init_embedding_warning(caplog):
    name = "problem.p"
    evaluation = {"a": 7, "b": -1}
    p = Problem(name, evaluation, embedding=[1, 1, 0])
    assert p.embedding == [1, 1, 0]
    assert "not supported" in caplog.text


@pytest.mark.parametrize(
    "inp, exp",
    [[("a", 10), (False, 10)], [("b", 2), (False, 2)], [("b", 8), (True, 8)]],
)
def test_problem_evaluate(inp, exp):
    evaluation = {"a": -1, "b": 8}
    p = Problem("p.p", evaluation)
    res = p.evaluate(*inp)
    assert res == exp


def test_problem_attempt_init():
    prob = Problem("a.p", {})
    attempt = ProblemAttempt(prob, False, 10, None)
    assert attempt.problem is prob
    assert attempt.name == "a.p"
    assert attempt.solved is False
    assert attempt.runtime == 10
    assert attempt.solved_by is None
    assert attempt.schedule is None
    assert str(attempt) == "Problem:a.p Solved:False"


def test_problem_attempt_set_schedule():
    prob = Problem("a.p", {})
    attempt = ProblemAttempt(prob, False, 10, None)
    assert attempt.schedule is None
    sched = [("h1", 100)]
    attempt.set_schedule(sched)
    assert attempt.schedule == sched


def test_get_problem_set():
    df = pd.DataFrame({"h1": [-1, 1, 2], "h2": [0, 4, 8]}, index=["p1", "p2", "p3"])
    problems = get_problem_set(df)
    assert problems[0].get_name() == "p1"
    assert problems[1].get_name() == "p2"
    assert problems[2].get_name() == "p3"

    assert problems[0].evaluation == {"h1": -1, "h2": 0}
    assert problems[1].evaluation == {"h1": 1, "h2": 4}
    assert problems[2].evaluation == {"h1": 2, "h2": 8}
