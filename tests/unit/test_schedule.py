import mock

from scpeduler.problem import Problem
from scpeduler.schedule import (
    print_schedule,
    report_schedule,
    remove_unallocated_heuristics,
    compute_order,
    Schedule,
)


def test_print_schedule_single_core():
    mock_schedule = mock.Mock()
    mock_schedule.schedule = [[("a", 10), ("b", 10)]]
    print_schedule(mock_schedule)


def test_print_schedule_multi_core():
    mock_schedule = mock.Mock()
    mock_schedule.schedule = [[("a", 10), ("b", 10)], [("c", 4)]]
    print_schedule(mock_schedule)


@mock.patch("scpeduler.schedule.print_schedule")
def test_report_schedule_deploy(mock_print):
    mock_schedule = mock.Mock()
    report_schedule(mock_schedule, True)
    mock_print.assert_called_once_with(mock_schedule)


@mock.patch("scpeduler.schedule.print_schedule")
def test_report_schedule_out(mock_print):
    mock_schedule = mock.Mock()
    report_schedule(mock_schedule, False)
    mock_print.assert_not_called()


def test_remove_unallocated_heuristics():
    time_pairs = {0: 0, 1: 1, 2: 10, 4: 73, 5: 0}
    res = remove_unallocated_heuristics(time_pairs)
    assert res == {1: 1, 2: 10, 4: 73}


def test_compute_order_all_zero(caplog):
    time_pairs = {0: 0}
    res = compute_order(time_pairs, 10)
    assert res == []
    assert "No heuristics" in caplog.text


def test_compute_order_single():
    time_pairs = {0: 2}
    res = compute_order(time_pairs, 10)
    assert res == [(0, 10)]


def test_compute_order_multiple():
    time_pairs = {0: 2, 1: 10, 2: 1, 4: 20}
    res = compute_order(time_pairs, 100)
    assert res == [(2, 1), (0, 2), (1, 10), (4, 20)]


def test_schedule_init():
    solution = {0: {"ha": 8}}
    sched = Schedule(solution, 10)
    assert sched.timeout == 10
    assert sched.schedule == [[("ha", 10)]]


# TODO need to test schedule attempt
# Not solved
# Solved
# Solved by a faster core later
# Solved - but timeout
# and increase to timeout?
def test_schedule_attempt_not_solved():
    solution = {0: {"ha": 10}, 1: {"hb": 10}}
    sched = Schedule(solution, 10)
    prob = Problem("p.p", {"ha": -1, "hb": 20})
    res = sched.attempt(prob)
    assert res.problem is prob
    assert not res.solved


def test_schedule_attempt_solved():
    solution = {0: {"ha": 10}, 1: {"hb": 10}}
    sched = Schedule(solution, 10)
    prob = Problem("p.p", {"ha": -1, "hb": 8})
    res = sched.attempt(prob)
    assert res.problem is prob
    assert res.solved
    assert res.runtime == 8
    assert res.solved_by == "hb"


def test_schedule_attempt_solved_single_core():
    solution = {0: {"ha": 10, "hb": 10}}
    sched = Schedule(solution, 20)
    prob = Problem("p.p", {"ha": -1, "hb": 8})
    res = sched.attempt(prob)
    assert res.problem is prob
    assert res.solved
    assert res.runtime == 18
    assert res.solved_by == "hb"


def test_schedule_attempt_solved_faster():
    solution = {0: {"ha": 10, "hb": 10}, 1: {"hc": 20}}
    sched = Schedule(solution, 20)
    prob = Problem("p.p", {"ha": -1, "hb": 8, "hc": 12})
    res = sched.attempt(prob)
    assert res.problem is prob
    assert res.solved
    assert res.runtime == 12
    assert res.solved_by == "hc"
