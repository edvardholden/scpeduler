import numpy as np
import pandas as pd
import pytest

from scpeduler.solver import Solver


def test_solver_init():
    workers = 2
    time = 300
    solver = Solver(workers, time)

    assert solver.solver is not None
    assert solver.solver.parameters.num_search_workers == workers
    assert solver.solver.parameters.max_time_in_seconds == time
    assert solver.evaluation_heuristics is None
    assert solver.evaluation_data is None


def test_solver_set_evaluation_data_int():
    df = pd.DataFrame({"H1": [-1, 2, 5], "H2": [-1, 7, 8], "H3": [-1, 1, 0]})
    solver = Solver(1, 10)
    solver.set_evaluation_data(df)
    assert solver.evaluation_heuristics == ["H1", "H2", "H3"]
    assert np.array_equal(solver.evaluation_data, np.array([[2, 7, 1], [5, 8, 0]]))


def test_solver_set_evaluation_data_float():
    df = pd.DataFrame({"H1": [-1, 2.4, 5.0], "H2": [-1, 7.1, 8.9], "H3": [-1, 1.01, 3]})
    solver = Solver(1, 10)
    solver.set_evaluation_data(df)
    assert solver.evaluation_heuristics == ["H1", "H2", "H3"]
    assert np.array_equal(solver.evaluation_data, np.array([[3, 8, 2], [5, 9, 3]]))


def test_solver_build_model_no_eval_data():
    solver = Solver(1, 10)
    with pytest.raises(AssertionError):
        solver.build_model(10, 10, 2)
