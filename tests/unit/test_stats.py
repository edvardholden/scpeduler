import mock
import numpy as np
import pandas as pd

from scpeduler.stats import (
    compute_top_n_heuristics,
    compute_solved_problems_heuristic,
    compute_greedy_min_cover,
    compute_universe,
    compute_set_cover,
    compute_solved_problems_upper_bound,
    compute_total_solving_time,
    compute_best_heuristic,
)


@mock.patch("scpeduler.stats.compute_solved_problems_heuristic")
def test_compute_top_n_heuristics(mock_solved):
    mock_solved.return_value = {
        "h1": {"p1"},
        "h2": {"p1", "p2"},
        "h3": {"p1", "p2", "p3", "p4"},
    }

    res = compute_top_n_heuristics(mock.Mock(pd.DataFrame), 3)
    assert res == [
        ("h3", {"p1", "p2", "p3", "p4"}),
        ("h2", {"p1", "p2"}),
        ("h1", {"p1"}),
    ]


@mock.patch("scpeduler.stats.compute_solved_problems_heuristic")
def test_compute_top_n_heuristics_single(mock_solved):
    mock_solved.return_value = {
        "h1": {"p1"},
        "h2": {"p1", "p2"},
        "h3": {"p1", "p2", "p3", "p4"},
    }

    res = compute_top_n_heuristics(mock.Mock(pd.DataFrame), 1)
    assert res == [
        ("h3", {"p1", "p2", "p3", "p4"}),
    ]


def test_compute_solved_problems_heuristic_none():
    df = pd.DataFrame([[-1, -1, -1], [-1, -1, -1]])
    res = compute_solved_problems_heuristic(df)
    assert res == {0: set(), 1: set(), 2: set()}


def test_compute_solved_problems_heuristic_various():
    df = pd.DataFrame([[-1, 0, 3], [-1, -1, 10]])
    res = compute_solved_problems_heuristic(df)
    assert res == {0: set(), 1: {0}, 2: {0, 1}}


@mock.patch("scpeduler.stats.compute_set_cover")
@mock.patch("scpeduler.stats.compute_universe")
@mock.patch("scpeduler.stats.compute_solved_problems_heuristic")
def test_compute_greedy_min_cover(mock_solved, mock_uni, mock_cover):
    mock_cover.return_value = (["h1", "h2"], mock.ANY)
    mock_uni.return_value = range(10)

    res = compute_greedy_min_cover(mock.Mock(pd.DataFrame))
    assert res[0] == ["h1", "h2"]
    assert res[1] == 10

    mock_solved.assert_called_once()
    mock_uni.assert_called_once()
    mock_cover.assert_called_once()


def test_compute_universe_empty():
    solved = {0: set(), 1: set(), 2: set()}
    res = compute_universe(solved)
    assert res == set()


def test_compute_universe_overlap():
    solved = {"0": {1, 2, 3}, "1": {4, 5, 6}, "2": {6, 7, 8}}
    res = compute_universe(solved)
    assert res == set(range(1, 9))


def test_compute_set_cover_all():
    universe = set(range(1, 9))
    solved = {0: {1, 2, 3}, 1: {4, 5, 6}, 2: {6, 7, 8}}
    res = compute_set_cover(universe, solved)
    assert res[0] == [0, 1, 2]
    assert res[1] == [3, 3, 2]


def test_compute_set_cover_all_but_one():
    universe = set(range(1, 8))
    solved = {0: {1, 2, 3, 4}, 1: {5, 6, 7}, 2: {1, 3, 7}}
    res = compute_set_cover(universe, solved)
    assert res[0] == [0, 1]
    assert res[1] == [4, 3]


def test_compute_solved_upper_bound():
    evaluation = [[1, -1, 0], [6, -1, 8], [-1, -1, 10]]
    df = pd.DataFrame(evaluation).T
    res = compute_solved_problems_upper_bound(df, 10)
    assert res == 2


def test_compute_solved_upper_bound_timeout():
    evaluation = [[-1, -1, -1], [6, -1, 8], [-1, -1, 10]]
    df = pd.DataFrame(evaluation)
    res = compute_solved_problems_upper_bound(df, 7)
    assert res == 1


def test_compute_total_solving_time():
    evaluation = [[-1, -1, -1], [6, -1, 8], [-1, -1, 10]]
    df = pd.DataFrame(evaluation)
    res = compute_total_solving_time(df)
    assert np.array_equal(res.values, [6.0, 0.0, 18.0])


# multiple solved - different times
# multiple different times
@mock.patch("scpeduler.stats.compute_total_solving_time")
@mock.patch("scpeduler.stats.compute_solved_problems_heuristic")
def test_compute_best_heuristic_single_top(mock_solved, mock_time):
    mock_solved.return_value = {0: set(range(9)), 1: set(range(3)), 4: set()}

    res = compute_best_heuristic(mock.Mock(pd.DataFrame))
    assert res == (0, set(range(9)))
    mock_solved.assert_called_once()
    mock_time.assert_not_called()


@mock.patch("scpeduler.stats.compute_total_solving_time")
@mock.patch("scpeduler.stats.compute_solved_problems_heuristic")
def test_compute_best_heuristic_single_fastest(mock_solved, mock_time):
    mock_solved.return_value = {"0": set(range(4)), "1": set(range(4)), "4": set()}
    mock_time.return_value = pd.Series([1, 300, 20], index=["0", "1", "4"])

    data = pd.DataFrame([[1, 1, 1]], columns=["0", "1", "4"])
    res = compute_best_heuristic(data)
    assert res == ("0", set(range(4)))
    mock_solved.assert_called_once()
    mock_time.assert_called_once()


@mock.patch("scpeduler.stats.compute_total_solving_time")
@mock.patch("scpeduler.stats.compute_solved_problems_heuristic")
def test_compute_best_heuristic_double_fastest(mock_solved, mock_time):
    mock_solved.return_value = {
        "0": set(range(4)),
        "1": set(range(4)),
        "4": set(range(2)),
    }
    mock_time.return_value = pd.Series([1, 1, 20], index=["0", "1", "4"])

    data = pd.DataFrame([[1, 1, 1]], columns=["0", "1", "4"])
    res = compute_best_heuristic(data)
    assert res == ("0", set(range(4)))
    mock_solved.assert_called_once()
    mock_time.assert_called_once()
